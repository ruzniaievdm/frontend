import React from 'react'
import classes from './Select.module.sass'

const Select = props => {
  const htmlFor = `${props.label}-${Date.now()}`

  return (
    <div className={classes.select}>
      <label htmlFor={htmlFor}></label>
      <select
        name=""
        id={htmlFor}
        value={props.value}
        onChange={props.onChange}
      >
        {props.options.map((opt, idx) => {
          return (
            <option
              value={opt.value}
              key={opt.value + idx}
            >
              {opt.text}
            </option>
          )
        })}
      </select>
    </div>
  )
}


export default Select