import React from 'react'
import classes from './Backdrop.module.sass'

const Backdrop = props => <div className={classes.Backdrop} onClick={props.onCLick}/>

export default Backdrop